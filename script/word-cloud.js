//$(document).ready(function(){
//    var width = 750,
//    height = 500;
//var fill = d3.scale.category20();
//d3.json("http://sports-api.empirics.com.au/v1/text/3", function(error, json) {
//    var words = json
//        .map(function(d) {return {text: d.key, size: +d.value}; })
//        .sort(function(a,b) {return d3.descending(a.size, b.size); });
//    
//    var layout = d3.layout.cloud()
//        .size([500, 500])
//        .words([words])
//        .padding(5)
//        .rotate(function() { return ~~(Math.random() * 2) * 90; })
//        .font("Impact")
//        .fontSize(function(d) { return d.size; })
//        .on("end", draw);
//    layout.start();
//});
//function draw(words) {
//  d3.select("#word-cloud").append("svg")
//      .attr("width", width)
//      .attr("height", height)
//    .append("g")
//      .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")")
//    .selectAll("text")
//      .data(words)
//    .enter().append("text")
//      .style("font-size", function(d) { return d.size + "px"; })
//      .style("font-family", "Impact")
//      .style("fill", function(d, i) { return fill(i); })
//      .attr("text-anchor", "middle")
//      .attr("transform", function(d) {
//        return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
//      })
//      .text(function(d) { return d.text; });
//}
//});








function wordCloud(scope, element, attrs) {

    var d3 = $(window).d3;

        
    
    
    var cloudSettings = {
            "async": true,
            "crossDomain": true,
            "url":"http://sports-api.empirics.com.au/v1/text/3",
            "headers": {
                "authorization": "09317bf6-ce06-436d-8835-a2b585b9c663"
            }
        }
            
    $.ajax(cloudSettings).done(function (response) {
        var tags = response;
             var fill = d3.scale.category20b();
    
        var w = $(window).innerWidth,
            h = $(window).innerHeight;

        var max, fontSize;

        var layout = d3.layout.cloud()
            .timeInterval(Infinity)
            .size([w, h])
            .fontSize(function (d) {
              return fontSize(+d.value);
            })
            .text(function (d) {
              return d.key;
            })
            .on("end", draw);

          var svg = d3.select(element[0]).append("svg")
            .attr("width", w)
            .attr("height", h);

          var vis = svg.append("g").attr("transform", "translate(" + [w >> 1, h >> 1] + ")");

          update();

          $(window).onresize = function (event) {
            update();
          };

          function draw(data, bounds) {
            var w = $window.innerWidth,
              h = $window.innerHeight;

            svg.attr("width", w).attr("height", h);

            var scale = bounds ? Math.min(
              w / Math.abs(bounds[1].x - w / 2),
              w / Math.abs(bounds[0].x - w / 2),
              h / Math.abs(bounds[1].y - h / 2),
              h / Math.abs(bounds[0].y - h / 2)) / 2 : 1;

            var text = vis.selectAll("text")
              .data(data, function (d) {
                return d.text.toLowerCase();
              });
            text.transition()
              .duration(1000)
              .attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
              })
              .style("font-size", function (d) {
                return d.size + "px";
              });
            text.enter().append("text")
              .attr("text-anchor", "middle")
              .attr("transform", function (d) {
                return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
              })
              .style("font-size", function (d) {
                return d.size + "px";
              })
              .style("opacity", 1e-6)
              .transition()
              .duration(1000)
              .style("opacity", 1);
            text.style("font-family", function (d) {
              return d.font;
            })
              .style("fill", function (d) {
                return fill(d.text.toLowerCase());
              })
              .text(function (d) {
                return d.text;
              });

            vis.transition().attr("transform", "translate(" + [w >> 1, h >> 1] + ")scale(" + scale + ")");
          }

          function update() {
            layout.font('impact').spiral('rectangular');
            fontSize = d3.scale['sqrt']().range([10, 100]);
            if (tags.length) {
              fontSize.domain([+tags[tags.length - 1].value || 1, +tags[0].value]);
            }
            layout.stop().words(tags).start();
          }
        
    });
}
