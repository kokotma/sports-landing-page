var chartIcons = {

    "Game": "fa fa-group",
    "Injury": "fa fa-exclamation-triangle",
    "Highlight": "fa fa-camera",
    "Switch": "glyphicon glyphicon-shopping-cart"

};

var indexScale;


function renderTimeline(dataTimeline, target, width) {
    
    

    // d3 boilerplate
    var margin = { top: 20, right: 30, bottom: 30, left: 50 };
    var width = $(target).parent().innerWidth() - margin.left - margin.right; //960 - margin.left - margin.right;
    var height = 200 - margin.top - margin.bottom;

    var x = d3.time.scale()
        .rangeRound([0, width]);

    var y = d3.scale.ordinal()
        .rangeRoundPoints([height, 0], 0);


    var svg = d3.select("#timeline-chart").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("class", "timeline-chart-svg")
    .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // Config values
    var minDate = dataTimeline.MinDate;
    var maxDate = dataTimeline.MaxDate;
    var eventTypes = dataTimeline.EventTypes;
    console.log(eventTypes);
    var maxIdx = dataTimeline.MaxIdx;

    // Set balance scale
    indexScale = d3.scale.linear()
                       .domain([0, maxIdx * 1.5])
                       .range([height, 0]);

    // Set input domains
    x.domain([minDate, maxDate]);
    y.domain(eventTypes);

    // Create some axis
    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom")
        .ticks(maxDate-minDate)
        .tickFormat(d3.format('.1'));

    // Draw the X Axis
    svg.append("g")
        .attr("class", "x axis x-axis")
        .attr('transform', 'translate(0, ' + (height) + ')')
        .call(xAxis);


    // Draw the Y Axis
    var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left");

    svg.append('g')
      .attr('class', 'y axis')
      .call(yAxis);

    function make_y_axis() {
        return d3.svg.axis()
            .scale(y)
            .orient("left")
            .ticks(4);
    }

    // Event Lines  	
    svg.append("g")
        .attr("class", "grid")
        .call(make_y_axis()
            .tickSize(-width, 0, 0)
            .tickFormat("")
    );
    

    // Contributions/Pension Payments
    var tip = d3.tip().attr('class', 'd3-tip').html(function (d) {
        var dt = parseInt(d.Date);
        function pad(s) { return (s < 10) ? '0' + s : s; };

        var format = d3.format("$,.0");

        // Add Dollar sign to value for conts.
        // Layout for tips
        var value = d.Value.trim().replace(/, /g, "<br/>").replace(/] /g, "]<br/>");
        var details = pad(dt)+ "<br/>";
        var subtype = d.SubType.trim().replace(/, /g, "<br/>").replace(/,/g, "").replace(/ \$/g, "<br/>$");
        if (d.Type.trim() == "Cont") {
            //value = format(value);
            //details += /*d.Type + "<br/>" + */d.SubType + "<br/>" + value;
            details += /*d.Type + "<br/>" + */subtype;// + "<br/>" + value;
        }
        else if (d.Type == "Call") {
            details += /*d.Type + "<br/>" + d.SubType + "<br/>" + */value;
        }
        else if (d.Type == "FP") {
            details += /*d.Type + "<br/>" + */d.SubType + "<br/>" + value;
        }
        else if (d.Type == "Pmt") {
            value = format(value);
            details += /*d.Type + "<br/>" + */d.SubType + "<br/>" + value;
        }
        else if (d.Type == "Balance") {
            value = format(value);
            details += /*d.Type + "<br/>" + */d.SubType + "<br/>" + value;
        }
        else {
            details += d.Type + "<br/>" + d.SubType + "<br/>" + value;
        }
        return details;

    })

    svg.call(tip);
    /*
    if (dataTimeline.MemberType == "PENSION") {
        svg.selectAll(".cont")
        .data(dataTimeline.Payments)
        .enter()
            .append("svg:foreignObject")
            .attr("transform", "translate(0,-7)")
            .attr("x", function (d) { return x(new Date(d.Date)); })
            .attr("y", function (d) { return y(d.Type); })
            .attr("height", "30px")
            .attr("width", "30px")
            // Mouse events need to go before the span, otherwise the tip shows up in the bottom left hand corner
            // mouseenter and mouseleave seem to work better, as mouseover and mouseout can get caught on previous icons
            .on('mouseenter', tip.show)
            .on('mouseleave', tip.hide)
            //.on('mouseover', tip.show)
            //.on('mouseout', tip.hide)
            .append("xhtml:span")
            .attr("class", "glyphicon glyphicon-arrow-down");
    }
    else {
        svg.selectAll(".cont")
        .data(dataTimeline.Contributions)
        .enter()
            .append("svg:foreignObject")
            .attr("transform", "translate(0,-7)")
            .attr("x", function (d) { return x(new Date(d.Date)); })
            .attr("y", function (d) { return y(d.Type); })
            .attr("height", "30px")
            .attr("width", "30px")
            // Mouse events need to go before the span, otherwise the tip shows up in the bottom left hand corner
            // mouseenter and mouseleave seem to work better, as mouseover and mouseout can get caught on previous icons
            .on('mouseenter', tip.show)
            .on('mouseleave', tip.hide)
            //.on('mouseover', tip.show)
            //.on('mouseout', tip.hide)
            .append("xhtml:span")
            .attr("class", "glyphicon glyphicon-usd");
    }
    */


    // Events    	
    svg.selectAll(".event")
        .data(dataTimeline.Events)
        .enter()
            .append("svg:foreignObject")
            .attr("transform", "translate(-8,-7)")
            .attr("x", function (d) { return x(parseInt(d.Date)); })
            .attr("y", function (d) { return y(d.SubType); })
            .attr("height", "16px")
            .attr("width", "16px")
            // Mouse events need to go before the span, otherwise the tip shows up in the bottom left hand corner
            // mouseenter and mouseleave seem to work better, as mouseover and mouseout can get caught on previous icons
            .on('mouseenter', tip.show)
            .on('mouseleave', tip.hide)
            //.on('mouseover', tip.show)
            //.on('mouseout', tip.hide)
            .append("xhtml:span")
            .attr("class", function (d) { return chartIcons[d.SubType]; });




    // Balance Points
    /*svg.selectAll(".balance")
        .data(dataTimeline.Balance)
        .enter()
        .append("circle")
            .attr("cx", function (d) { return x(new Date(d.Date)); })
            .attr("cy", function (d) { return balanceScale(d.Value); })
            .attr("r", "4")
            .attr("style", "")//;
            .on('mouseenter', tip.show)
            .on('mouseleave', tip.hide);*/

    // Balance Line
    var line = d3.svg.line()
        .x(function (d) { return x(parseInt(d.Date)); })
        .y(function (d) { return indexScale(d.Value); });

    svg.append("path")
      .datum(dataTimeline.Wellness)
      .attr("class", "wellness-line")
      .attr("d", line);

    svg.append("path")
      .datum(dataTimeline.Performance)
      .attr("class", "performance-line")
      .attr("d", line);
    
/*
    // Balance Points
    svg.selectAll(".wellness-point")
        .data(dataTimeline.Wellness)
        .enter()
        .append("circle")
            .attr("cx", function (d) { return x(parseInt(d.Date)); })
            .attr("cy", function (d) { return indexScale(d.Value); })
            .attr("r", "2")
            .attr("style", "");
            //.on('mouseenter', tip.show)
            //.on('mouseleave', tip.hide);

    svg.selectAll(".performance-point")
        .data(dataTimeline.Performance)
        .enter()
        .append("circle")
            .attr("cx", function (d) { return x(parseInt(d.Date)); })
            .attr("cy", function (d) { return indexScale(d.Value); })
            .attr("r", "2")
            .attr("style", "");
            //.on('mouseenter', tip.show)
            //.on('mouseleave', tip.hide);

    // Balance Points
    svg.selectAll(".wellness-point")
        .data(dataTimeline.Wellness)
        .enter()
        .append("circle")
            .attr("cx", function (d) { return x(parseInt(d.Date)); })
            .attr("cy", function (d) { return indexScale(d.Value); })
            .attr("r", "10")
            .attr("fill-opacity", "0.0")//;
            .attr("style", "")//;
            .on('mouseenter', tip.show)
            .on('mouseleave', tip.hide);

    svg.selectAll(".performance-point")
        .data(dataTimeline.Performance)
        .enter()
        .append("circle")
            .attr("cx", function (d) { return x(parseInt(d.Date)); })
            .attr("cy", function (d) { return indexScale(d.Value); })
            .attr("r", "10")
            .attr("fill-opacity", "0.0")//;
            .attr("style", "")//;
            .on('mouseenter', tip.show)
            .on('mouseleave', tip.hide);
*/

}